# `casual-mounts`: mounting made easier

`casual-mounts` is about mounting filesystems / images conveniently
* from the command line
* in boot scripts, when a specific sequence of steps is required (e.g. bind-mount, then mount a loop image in the bind, then bind a directory in the image).

This page briefly lists what is available &mdash; see the wiki pages (linked from headings) for detailed usage.

## CLI tools

### `fusemnt+run`

Somewhat akin to `AppImage`: mounts a squash filesystem, runs an entry point script, and cleans up when the entry point exits

### `tmpmnt` / `umount_tmp`

`tmpmnt` is a combination of `findmnt`, `mkdir`, and `mount`: it looks up an existing mountpoint (or creates a new one) for an image / device (can be specified as a partition label), mounts the image with sensible defaults, and prints the mountpoint.

`umount_tmp` tries harder to unmount volumes and loops, breaking dependencies that prevent normal `umount` from working.

## System mount setup / tear-down

### [`mntlist.sh`](https://gitlab.com/kstr0k/casual-mounts/-/wikis/Frameworks/mntlist.sh)

Automates a sequence of mount steps to bring the system to a required state, and possibly runs them in reverse at system shutdown. See [mntlist-demo](doc/mntlist-demo.sh).

## Copyright

`Alin Mr. <almr.oss@outlook.com>` / MIT license
