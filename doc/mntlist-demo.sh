#!/bin/bash
set -xeuo pipefail
shopt -s inherit_errexit

# e.g.: demo.sh mnt; demo.sh umnt

. ~/src/casual-mounts/lib/mntlist.sh --
__mntlist_helpers

__mntlist_add $(cat <<'EOMNT'
/dosc   __mw2r
/opt/go __mr
EOMNT
)
declare -p __mntlist __mntlist_mp

action=${1?no action; try mnt, umnt}
("__mntlist_$1" "${@:2}" )
echo 1>&2 'Done'
