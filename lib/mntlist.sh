#!/bin/bash
# source-me

declare -a __mntlist_mp_act

# local -n __mntlist_mp_act=... in surrounding func() for different global arrays

__mntlist_add() {  # args: mountpoint actions
  while (( $# > 0 )); do
    if (( $# == 1 )); then unset Bad; ${Bad?args uneven}; fi
    __mntlist_mp_act+=( "$1"$'\n'"$2" )
    shift 2
  done
}

____mntlist_revarr() {  # reverse $2... into $1 (set to () beforehand)
  test a = "$1" || local -n a=$1; shift
  local i; for i; do a=( "$i" "${a[@]}" ); done
}

__mntlist_mnt_step() { "$@"; }
__mntlist_mnt() {
  local action= reverse=false mntargs=  # keyword args
  local mpas=() mp mpa "$@"

  if "${reverse:=false}"; then
    ____mntlist_revarr mpas "${__mntlist_mp_act[@]}"
  else mpas=( "${__mntlist_mp_act[@]}" )
  fi
  for mp in "${mpas[@]}"; do
    mpa=${mp#*$'\n'}; mp=${mp%%$'\n'*}
    case "$action" in
      *'()') mpa=$(${action%'()'} "$mpa") ;;
      '') ;;
      *) mpa=$action ;;
    esac
    __mntlist_mnt_step $mpa "$mp" $mntargs
  done
}
__mntlist_umnt() { __mntlist_mnt action=umount reverse=true "$@"; }

__mntlist_helpers() {  # call to declare helpers
  __ism()  { mountpoint -q "$1"; }
  __m()    { __ism "$1" || mount "$@"; }
  __mr()   { __ism "$1" || mount -r "$@"; }
  __mw()   { __ism "$1" || mount -w "$@"; }
  __remw() { mount -o remount -w "$@"; }
  __remr() { mount -o remount -r "$@"; }
  __mw2r() {
    ! __ism "$1" || return 0
    __mw "$@"
    __remr "$@"
  }
  __mntlist_undo_act() {
    local u=umount
    case "$1" in
      __remw) u=__remr ;;
      __remr) u=: ;;
    esac
    printf '%s\n' "$u"
  }
  __mntlist_umnt() { __mntlist_mnt action='__mntlist_undo_act()' reverse=true "$@"; }
}
