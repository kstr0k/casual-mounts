#!/bin/bash -neu
_bashaaparse_chkver () {
  [[ ${BASH_VERSINFO[0]} -gt 4 ]] || [[ ${BASH_VERSINFO[0]} -eq 4 && ${BASH_VERSINFO[1]} -ge 4 ]] ||
    { echo "bash too old"; return 1; }
  local _BASHAAPARSE_VERSION=1.2  # MAJOR.minor.patch
  local v=${1#----v}
  test "$1" = "$v" && return 0
  local vpat='^([^.]+).([^.+])'
  [[ $_BASHAAPARSE_VERSION =~ $vpat ]] || return 1; local pv=( "${BASH_REMATCH[@]}" )
  [[ $v =~ $vpat ]]                       || return 1; local rv=( "${BASH_REMATCH[@]}" )
  [[ ${rv[1]} -eq ${pv[1]} && ${rv[2]} -le ${pv[2]} ]] ||
    { echo 1>&2 "ERROR: mismatched argparse version (need $v, got $_BASHAAPARSE_VERSION)"; return 1; }
  return 0
}
_bashaaparse_chkver "$@"

# --- x8 --- start here to embed script ---

_amember () {
  local h; local ndl=$1; shift
  for h; do test "$ndl" = "$h" && return 0; done
  return 1
}

_aappend () {
  test "$1" = "_f" || local -n _f=$1
  test "$2" = "_t" || local -n _t=$2
  local k
  for k in "${!_f[@]}"; do _t["$k"]=${_f["$k"]}; done
}

_process_switch () {
  test "$1" = "_n" || local -n _n=$1; shift; _n=0
  case "$1" in -h|--help) _print_help; exit 0 ;; esac
  local s=${1#--}
  if _amember "$s" "${!CLI_OPTS_bool[@]}"; then  # TODO: --arg=val
    _n=1; CLI_OPTS_bool["$s"]=y
  elif _amember "${s#no-}" "${!CLI_OPTS_bool[@]}"; then
    _n=1; CLI_OPTS_bool["${s#no-}"]=
  elif   _amember "$s" "${!CLI_OPTS[@]}"; then
    _n=2; CLI_OPTS["$s"]="$2"
  else return 1
  fi; return 0
}

_print_help () {
local s
cat <<EOF
Usage: ${_USAGE:-$(basename "$0") [<option> ...] [...]}

Options:
     --help
EOF
for s in "${!CLI_OPTS[@]}"; do cat <<EOF
     --$s <$s>
EOF
done
for s in "${!CLI_OPTS_bool[@]}"; do cat <<EOF
     --$s
  --no-$s
EOF
done
}

_read_cfg () {
  test -r "$1" || return 1
  local kv; local _n
  while IFS= read -r kv; do
    _process_switch _n "${kv%%=*}" "${kv#*=}"
  done <"$1"
}
# end bashaaparse


# vim: set ft=bash:
